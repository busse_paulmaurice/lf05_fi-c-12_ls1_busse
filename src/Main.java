public class Main {

    public static void main(String[] args) {
        Raumschiff klingonen = new Raumschiff();
        Raumschiff romulaner = new Raumschiff();
        Raumschiff vulkanier = new Raumschiff();

        Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
        Ladung l2 = new Ladung("Bat´lezh Klingonen Schwert", 200);
        Ladung l3 = new Ladung("Borg Schrott", 5);
        Ladung l4 = new Ladung("Rote Materie", 2);
        Ladung l5 = new Ladung("Plasma Waffe", 50);
        Ladung l6 = new Ladung("Forschungssonde", 35);
        Ladung l7 = new Ladung("Photonentorpedo", 3);

        klingonen.setPhotonentorpedoAnzahl(1);
        klingonen.setEnergieversorgungInProzent(100);
        klingonen.setSchildInProzent(100);
        klingonen.setHuelleInProzent(100);
        klingonen.setLebenserhaltungssystemInProzent(100);
        klingonen.setSchiffsname("IKS Hegh´ta");
        klingonen.setAndroidenAnzahl(2);
        klingonen.addLadung(l1);
        klingonen.addLadung(l2);

        romulaner.setPhotonentorpedoAnzahl(2);
        romulaner.setEnergieversorgungInProzent(100);
        romulaner.setSchildInProzent(100);
        romulaner.setHuelleInProzent(100);
        romulaner.setLebenserhaltungssystemInProzent(100);
        romulaner.setSchiffsname("IRW Khazara");
        romulaner.setAndroidenAnzahl(2);
        romulaner.addLadung(l3);
        romulaner.addLadung(l4);
        romulaner.addLadung(l5);

        vulkanier.setPhotonentorpedoAnzahl(0);
        vulkanier.setEnergieversorgungInProzent(80);
        vulkanier.setSchildInProzent(80);
        vulkanier.setHuelleInProzent(50);
        vulkanier.setLebenserhaltungssystemInProzent(100);
        vulkanier.setSchiffsname("Ni´Var");
        vulkanier.setAndroidenAnzahl(5);
        vulkanier.addLadung(l6);
        vulkanier.addLadung(l7);

        System.out.println("Zustand der Schiffe");
        System.out.println("_____________________");
        System.out.println(klingonen.getSchiffsname());
        klingonen.zustandRaumschiff();
        System.out.println("~~~~~~~~~~~~~");
        System.out.println(romulaner.getSchiffsname());
        romulaner.zustandRaumschiff();
        System.out.println("~~~~~~~~~~~~~");
        System.out.println(vulkanier.getSchiffsname());
        vulkanier.zustandRaumschiff();
        System.out.println("---------------------");

        System.out.println("Ladungsverzeichnis der Schiffe");
        System.out.println("_____________________");
        System.out.println(klingonen.getSchiffsname());
        klingonen.ladungsverzeichnisAusgeben();
        System.out.println("~~~~~~~~~~~~~");
        System.out.println(romulaner.getSchiffsname());
        romulaner.ladungsverzeichnisAusgeben();
        System.out.println("~~~~~~~~~~~~~");
        System.out.println(vulkanier.getSchiffsname());
        vulkanier.ladungsverzeichnisAusgeben();
        System.out.println("_____________________");
        System.out.println(" ");


        vulkanier.photonentorpedosLaden(3);
        vulkanier.phaserkanoneSchiessen(romulaner);
        vulkanier.photonentorpedoSchiessen(romulaner);
        vulkanier.ladungsverzeichnisAusgeben();
        vulkanier.ladungsverzeichnisAufräumen();
        vulkanier.ladungsverzeichnisAusgeben();
        vulkanier.zustandRaumschiff();
    }
}
