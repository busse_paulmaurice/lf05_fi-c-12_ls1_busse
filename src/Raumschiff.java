import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {
    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemInProzent;
    private int androidenAnzahl;
    private String schiffsname;
    private final ArrayList<String> broadcastKommunikator = new ArrayList<>();
    private final ArrayList<Ladung> ladungsverzeichnis = new ArrayList<>();

    public Raumschiff() {
    }

    public int getPhotonentorpedoAnzahl() {
        return photonentorpedoAnzahl;
    }

    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    }

    public int getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }

    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }

    public int getSchildInProzent() {
        return schildInProzent;
    }

    public void setSchildInProzent(int schildInProzent) {
        this.schildInProzent = schildInProzent;
    }

    public int getHuelleInProzent() {
        return huelleInProzent;
    }

    public void setHuelleInProzent(int huelleInProzent) {
        this.huelleInProzent = huelleInProzent;
    }

    public void setLebenserhaltungssystemInProzent(int lebenserhaltungssystemInProzent) {
        this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
    }

    public int getAndroidenAnzahl() {
        return androidenAnzahl;
    }

    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    public String getSchiffsname() {
        return schiffsname;
    }

    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }

    /**
     * Fügt neue Ladung hinzu
     * @param neueLadung Ladung, die hinzugefügt wird
     */
    public void addLadung(Ladung neueLadung) {
        ladungsverzeichnis.add(neueLadung);
    }

    /**
     * Torpedoschuss wird abgefeuert
     * @param ziel Was vom Torpedo getroffen werden soll
     */
    public void photonentorpedoSchiessen(Raumschiff ziel) {
        if (photonentorpedoAnzahl == 0) {
            nachrichtAnAlle("-=*Click*=-");
        } else {
            photonentorpedoAnzahl -= 1;
            nachrichtAnAlle("Photonentorpedo abgeschossen");
            treffer(ziel);
        }
    }

    /**
     * Phaserkanonenschuss wird abgefeuert
     * @param ziel Was von der Phaserkanone getroffen werden soll
     */
    public void phaserkanoneSchiessen(Raumschiff ziel) {
        if (energieversorgungInProzent < 50) {
            nachrichtAnAlle("-=*Click*=-");
        } else {
            energieversorgungInProzent -= 50;
            nachrichtAnAlle("Phaserkanone Abgeschossen");
            treffer(ziel);
        }
    }

    /**
     * Geschehen nach dem Treffer
     * @param r Raumschiff, welches getroffen wird/wurde
     */
    private void treffer(Raumschiff r) {
        int zwischenSpeicher;
        System.out.println(r.getSchiffsname() + " wurde getroffen");
        zwischenSpeicher = r.getSchildInProzent() - 50;
        if (zwischenSpeicher <= 0) {
            r.setSchildInProzent(0);
            zwischenSpeicher = r.getEnergieversorgungInProzent() - 50;
            r.setEnergieversorgungInProzent(Math.max(zwischenSpeicher, 0));
            zwischenSpeicher = r.getHuelleInProzent() - 50;
            if (zwischenSpeicher < 0) {
                r.setHuelleInProzent(0);
                r.setLebenserhaltungssystemInProzent(0);
                r.nachrichtAnAlle("Lebenserhaltungssytem wurde vernichtet");
            } else {
                r.setHuelleInProzent(zwischenSpeicher);
            }

        }

    }

    /**
     * Nachricht an wird an alle geschickt
     * @param message Nachrichten-Inhalt
     */
    public void nachrichtAnAlle(String message) {
        System.out.println(message);
        broadcastKommunikator.add(message);
    }

    /**
     * Einträge aus dem Logbuch werden zurückgegeben
     * @return Inhalt wird zurückgegeben
     */
    public ArrayList<String> eintraegeLogbuchZurueckGeben() {
        return broadcastKommunikator;
    }

    /**
     * Torpedos nachladen
     * @param anzahlTorpedos Torpedoanzahl, die nachgeladen wird
     */
    public void photonentorpedosLaden(int anzahlTorpedos) {
        String speicher;
        int menge = 0;
        int torpedos;
        int zahl = 0;
        int index = 0;
        for (int i = 0; i < ladungsverzeichnis.size(); i++) {
            speicher = ladungsverzeichnis.get(i).getBezeichnung();
            if (speicher.equals("Photonentorpedo")) {
                menge = ladungsverzeichnis.get(i).getMenge();
                index = i;
                // Speichern der Position der Torpedos im Ladungsverzeichnis
                zahl = i;
            }
        }
        if (menge == 0) {
            System.out.println("Keine Photonentorpedo Gefunden!");
            nachrichtAnAlle("-=*Click*=-");
        } else {
            torpedos = menge - anzahlTorpedos;
            if (torpedos < 0) {
                torpedos = getPhotonentorpedoAnzahl();
                setPhotonentorpedoAnzahl(torpedos + menge);
                ladungsverzeichnis.get(zahl).setMenge(0);
            } else {
                torpedos = getPhotonentorpedoAnzahl();
                setPhotonentorpedoAnzahl(torpedos + anzahlTorpedos);
                ladungsverzeichnis.get(zahl).setMenge(menge - anzahlTorpedos);
            }
        }
        ladungsverzeichnisAufräumen();
    }

    /**
     * Reperatur-Durchführung
     * @param schutzschilde Prüfen, ob Schutzschilde werden wieder hergestellt müssen
     * @param energieversorgung Prüfen, ob Energieversorgung wird wieder hergestellt muss
     * @param schiffshuelle Prüfen, ob Schiffshülle wird wieder hergestellt muss
     * @param anzahlDroiden Anzahl der Droiden, die die Reperatur duchführen
     */
    public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
        Random zufall = new Random();
        int counter = 0;
        int ergebnis = 0;
        int zahl = zufall.nextInt(100);
        if (schutzschilde) {
            counter += 1;
        }
        if (energieversorgung) {
            counter += 1;
        }
        if (schiffshuelle) {
            counter += 1;
        }
        if (anzahlDroiden > getAndroidenAnzahl()) {
            ergebnis = zahl * getAndroidenAnzahl() / counter;
        }
        if (anzahlDroiden <= getAndroidenAnzahl()) {
            ergebnis = zahl * anzahlDroiden / counter;
        }
        if (schutzschilde) {
            setSchildInProzent(getSchildInProzent() + ergebnis);
            if (getSchildInProzent() > 100) {
                setSchildInProzent(100);
            }
        }
        if (energieversorgung) {
            setEnergieversorgungInProzent(getEnergieversorgungInProzent() + ergebnis);
            if (getEnergieversorgungInProzent() > 100) {
                setEnergieversorgungInProzent(100);
            }
        }
        if (schiffshuelle) {
            setHuelleInProzent(getHuelleInProzent() + ergebnis);
            if (getHuelleInProzent() > 100) {
                setHuelleInProzent(100);
            }
        }

    }

    /**
     * Ausgabe über den Zustand des Raumschiffs
     */
    public void zustandRaumschiff() {
        System.out.println("Schild:                 " + schildInProzent + "%");
        System.out.println("Hüller:                 " + huelleInProzent + "%");
        System.out.println("Lebenserhaltungssystem: " + lebenserhaltungssystemInProzent + "%");
        System.out.println("Energie:                " + energieversorgungInProzent + "%");
        System.out.println("Androiden Anzahl:       " + androidenAnzahl);
        System.out.println("Photonentorpedo Anzahl: " + photonentorpedoAnzahl + " Stück");
    }

    /**
     * Ausgabe des Ladungsverzeichnisses
     */
    public void ladungsverzeichnisAusgeben() {

        for (Ladung ladung : ladungsverzeichnis) {
            System.out.println(ladung.getBezeichnung() + ": " + ladung.getMenge());
        }
    }

    /**
     *  Ladungsverzeichnis wird aufgeräumt
     */
    public void ladungsverzeichnisAufräumen() {
        int save;
        for (int i = 0; i < ladungsverzeichnis.size(); i++) {
            save = ladungsverzeichnis.get(i).getMenge();
            if (save == 0) {
                ladungsverzeichnis.remove(i);
            }
        }
    }

    /**
     * Ausgabe über alle Bestandteile, Ladung, Zustand, etc. über das Raumschiff
     * @return Ausgabe wird durchgeführt (ausgegeben)
     */
    @Override
    public String toString() {
        return "Raumschiff{" + "photonentorpedoAnzahl=" + photonentorpedoAnzahl + ", energieversorgungInProzent=" + energieversorgungInProzent + ", schildInProzent=" + schildInProzent + ", huelleInProzent=" + huelleInProzent + ", lebenserhaltungssystemInProzent=" + lebenserhaltungssystemInProzent + ", androidenAnzahl=" + androidenAnzahl + ", schiffsname='" + schiffsname + '\'' + ", broadcastKommunikator=" + broadcastKommunikator + ", ladungsverzeichnis=" + ladungsverzeichnis + '}';
    }
}