public class Ladung {
    private final String bezeichnung;
    private int menge;

    /**
     * Konstruktor um Werte zu initialisieren
     * @param bezeichnung Bezeichnung wird initialisiert
     * @param menge Menge wird initialisiert
     */
    public Ladung(String bezeichnung, int menge) {
        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    /**
     * Werte werden zum String geändert
     * @return Ausgabe der Ladung (Ladungsbezeichnung + Ladungsmenge)
     */
    @Override
    public String toString() {
        return "Ladung{" + "bezeichnung='" + bezeichnung + '\'' + ", menge=" + menge + '}';
    }
}